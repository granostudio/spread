<?php
/*
 Plugin Name: Grano Blog
 Plugin URI: http://www.granostudio.com.br
 Description: Blog
 Author: Grano Studio
 Version: 1.0.0
 Author URI: http://www.granostudio.com.br
 */
 /*--------------------------------------------------------------
  *			Register Grano Plugin
  *-------------------------------------------------------------*/

	add_action( 'wp_loaded', 'ativarPluginBlog' );
	function ativarPluginBlog(){
		// $ativar = new GranoPluginsAtivos();
		// $ativar->ativar('Blog', 'Blog');
		add_action( 'admin_menu', 'add_admin_menus' );
		add_action( 'wp_before_admin_bar_render', 'add_toolbar_menus', 999 );

		function add_admin_menus() {
					add_menu_page(
					__( 'Posts', 'granostudio' ),
					'Posts',
					'Post',
					'edit.php',
					'',
					'dashicons-admin-post',
					1 );
		}

		function add_toolbar_menus() {
			global $wp_admin_bar;
			$args = array(
				'id'     => 'new-post',     // id of the existing child node (New > Post)
				'title'  => 'Post', // alter the title of existing node
				'parent' => 'new-content',
				'href'   =>  'post-new.php',
			);
			$wp_admin_bar->add_node( $args );
		}

		// metabox blog/post
		add_action( 'cmb2_init', 'cmb2_post' );
		/**
		 * Define the metabox and field configurations.
		 */
		function cmb2_post() {

				// Start with an underscore to hide fields from custom fields list
				$prefix = 'post_';

				/**
				 * Initiate the metabox
				 */
				$cmb = new_cmb2_box( array(
						'id'            => 'post_SEO',
						'title'         => __( 'SEO', 'cmb2' ),
						'object_types'  => array( 'post', ), // Post type
						'context'       => 'side',
						'priority'      => 'low',
						// 'show_names'    => true, // Show field names on the left
						// 'cmb_styles' => false, // false to disable the CMB stylesheet
						// 'closed'     => true, // Keep the metabox closed by default
				) );

				$cmb->add_field( array(
						'name'    => 'Facebook Image',
						'desc'    => 'Formato 1200px x 630px',
						'id'      => $prefix.'_seo_faceimage',
						'type'    => 'file',
						// Optionally hide the text input for the url:
						'options' => array(
								'url' => false,
						),
				) );

		}

	}
/*--------------------------------------------------------------
 *			Register Slider Post Type
 *-------------------------------------------------------------*/
