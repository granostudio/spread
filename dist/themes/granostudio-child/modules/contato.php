<?php

/**
* Módulo:
* ***** Contato - Page Template *****
*
* @package WordPress
* @subpackage Grano Studio
* @since Grano Studio 1.0
 */

function module_contato($contato,$key){
  ?>
  <div id="contato" class="container contato-<?php echo $key; ?>">
    <div class="row">
      <div class="col-lg-8 col-lg-offset-2">
          <?php echo do_shortcode('[contact-form-7 id="'.$contato.'"]'); ?>
      </div>
    </div>
  </div>
  <?php
}
 ?>
