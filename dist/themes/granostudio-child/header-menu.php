<!-- NAV BAR ============================================================== -->

<nav class="navbar fixed-top navbar-expand-lg navbar-light  justify-content-between" role="navigation"
      ng-class="{dark:scroll >394}">

    <!-- Brand and toggle get grouped for better mobile display -->

      <!-- <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button> -->
      <a class="navbar-brand" href="<?php echo get_home_url(); ?>">
        <div class="logo-dark" style="background-image:url('<?php echo get_stylesheet_directory_uri() ?>/img/header-logo-preto.png')"></div>
          <?php
          $logo_image_url = grano_get_options('design','design_logo');
          if(empty($logo_image_url) || $logo_image_url == "http://default"){
            echo bloginfo('name');

          }else{
            echo '<img src="'.$logo_image_url.'" alt="'.get_bloginfo('name').'" class="navbar-brand-img" />';
          }
           ?>
        </a>
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link newsletter btn-spread btn-spread-dark" href="#">faça parte de nosso newsletter  <span></span></a>
          </li>
          <!-- <li class="nav-item">
            <a class="nav-link " href="#">Blog</a>
          </li> -->
          <!-- <li class="nav-item">
            <a class="nav-link" href="#">Search</a>
          </li> -->
        </ul>

</nav>
<!-- /NAV BAR ============================================================== -->
