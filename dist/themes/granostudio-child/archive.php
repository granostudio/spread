<?php
/**
 * The template for displaying archive pages
 *
 *
 */

get_header(); ?>

<!-- Page Content -->

<div class="blog blog-home blog-single archive" >
	<span class='scroll-debug' scroll-position="scroll"></span>
  <div class="blog-banner" >
    <div class="content">
      <h1><?php
      the_archive_title( '<small>', '</small><br/>' );
      the_archive_description( '<small>', '</small>' );
       ?></h1>
    </div>
    <div class="mask">
      <svg
     version="1.1"
     id="no-aspect-ratio"
     xmlns="http://www.w3.org/2000/svg"
     xmlns:xlink="http://www.w3.org/1999/xlink"
     x="0px"
     y="0px"
     height="100%"
     width="100%"
     viewBox="0 0 100 100"
     preserveAspectRatio="none">
        <polygon fill="white" points="100,100 0,100 0,0 "/>
      </svg>
    </div>
  </div>

    <div class="container">

        <div class="row loop">
          <div class="col-12">
            <div class="row">
            <div class="col-12 loop-2">
      				<div class="row">
      					<?php
      					//Protect against arbitrary paged values


      					if ( have_posts() ) {
      						while ( have_posts() ) {
      							the_post();
      							?>
      							<div class="col-12 col-md-6 col-lg-4">
      								<?php if ( has_post_thumbnail() ) {
      									?>
      										<div class="post" style="background-image:url(<?php the_post_thumbnail_url(); ?>)">
      									<?php
      								}else {
      									?>
      										<div class="post" >
      										<!-- <div class="post" style="background-image:url(http://via.placeholder.com/400x300)"> -->
      									<?php
      								} ?>
      									<a href="<?php echo get_permalink(); ?>" class="link-post"></a>
      									<div class="content">
      										<div class="cat">
      											<?php the_category(', '); ?>
      										</div>
      										<h2>
      											<?php echo get_the_title(); ?>
      										</h2>
      										<p><?php the_excerpt() ?></p>
      										<div class="footer">
      											<div class="redes-sociais">
      												<a class="face icon icon-social-facebook">Facebook</a>
      												<a class="twitter icon icon-social-twitter">Twitter</a>
      												<a class="linkedin icon icon-social-linkedin">LinkedIn</a>
      											</div>
      											<a href="<?php echo get_permalink(); ?>" class="btn-lg btn-spread icon-seta">
      												saiba mais
      												<span></span>
      											</a>
      										</div>
      									</div>
      								</div>
      							</div>

      							<?php
      						} // end while
      					} // end if
      					?>
      				</div>

      			</div>
      		</div>
          </div>
        </div>
      <div class="row">
        <div class="col-12">
          <nav aria-label="..." class="paginacao">
        		<ul class="pagination justify-content-center">
        			<!-- <li class="page-item">
        	      <a class="page-link icon-seta" href="<?php // echo get_previous_posts_link('Previous', $query->max_num_pages); ?>" aria-label="Previous">
        	        <span class="sr-only">Previous</span>
        	      </a>
        	    </li> -->

        	<?php
        		$big = 999999999; // need an unlikely integer

        		echo paginate_links( array(
        			'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
        			'format' => '?paged=%#%',
        			'current' => max( 1, get_query_var('paged') ),
        			'prev_next'          => true,
        			'prev_text'          => __('<span class="sr-only">Previous</span>'),
        			'next_text'          => __('<span class="sr-only">Next</span>'),
        			'before_page_number' => '<li class="page-item">',
        			'after_page_number'  => '</li>'
        		) );
        		?>
        	  </ul>
        	</nav>
        </div>
      </div>
    </div>

<?php get_footer(); ?>
