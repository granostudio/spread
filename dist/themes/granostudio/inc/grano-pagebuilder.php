<?php
function meta_box_markup($object)
{
    wp_nonce_field(basename(__FILE__), 'page-builder');


    ?>
        <div class="pbg" ng-app="pgb" ng-controller="pgbCtrl" >
          <div gridster="gridsterOpts">
            <ul>
                <li style="border: solid 1px;" gridster-item="item" ng-repeat="item in standardItems track by $index" >
                  <div class="ddd" style="width: 100%; background: #ddd; padding: 10px 0px">
                    <md-button class="md-raised" ng-click="removeItem($index)">remover</md-button>
                    <md-menu >
                      <md-button aria-label="Open list plugins" class="md-raised" ng-click="$mdMenu.open($event)">
                        Add. Conteúdo
                      </md-button>
                      <md-menu-content width="6">
                        <md-menu-item ng-repeat="choiceitem in choiceitens track by $index">
                          <md-button ng-click=""> {{choiceitem.type}} </md-button>
                        </md-menu-item>
                      </md-menu-content>
                    </md-menu>
                  </div>
                    {{ item }}
                    {{ item.data }}
                </li>
            </ul>
        </div>
        <form class="form-inline" name="formItem">
          <md-button lass="md-raised" ng-click="adicionaItem()">adicionar ítem</md-button>
        </form>
        </div>
        <textarea name="complete_layout_data" id="complete_layout_data" style="display: block" ></textarea>
        <?php echo json_encode(get_post_meta($object->ID, 'complete_layout_data', true)); ?>

    <?php
}

function my_custom_meta_box()
{
    add_meta_box("page-builder", __( 'Page Builder', 'granoexpresso' ), "meta_box_markup", "page");
}
add_action("add_meta_boxes", "my_custom_meta_box");
add_action('edit_form_after_title', function() {
    global $post, $wp_meta_boxes;
    do_meta_boxes(get_current_screen(), 'advanced', $post);
    unset($wp_meta_boxes[get_post_type($post)]['advanced']);
});


function save_my_custom_meta_box($post_id,  $post, $update)
{
    if (!isset($_POST["page-builder"]) || !wp_verify_nonce($_POST["page-builder"], basename(__FILE__)))
      return $post_id;

    if(!current_user_can("edit_post", $post_id))
        return $post_id;

    if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
        return $post_id;

    $slug = "page";
    if($slug != $post->post_type)
        return;


    $complete_layout_data = "";
    if(isset($_POST["complete_layout_data"]))
    {
        $complete_layout_data = $_POST["complete_layout_data"];
    }
    else
    {
        $complete_layout_data = "";
    }
    update_post_meta($post_id, "complete_layout_data", $complete_layout_data);

}

add_action("save_post", "save_my_custom_meta_box", 10, 3);
