<?php
/**
 * The template for displaying all single portfolio and attachments
 *
 * @package WordPress
 * @subpackage Grano Studio
 * @since Grano Studio 1.0
 */

get_header(); ?>

<!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Post Content Column -->
            <div class="col-lg-8 col-sm-offset-2 ">

                <!-- Blog Post -->

								<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

                <!-- Title -->
                <h1><?php echo get_the_title(); ?></h1>

                <!-- Author -->
                <p class="lead">
                    by <a href="<?php echo get_author_posts_url(get_the_author_meta( 'ID' )); ?>"><?php echo get_the_author(); ?></a>
                </p>

                <hr>

                <!-- Date/Time -->
								<?php $data_atualização = get_the_modified_date(); ?>
                <p><span class="glyphicon glyphicon-time"></span> <?php echo get_the_date(); ?> | <?php echo !empty($data_atualização) ? "Atualizado dia ".$data_atualização : ''; ?></p>

                <hr>

								<?php
								// post thumbanil
                $galeria = get_post_meta(get_the_ID(), 'portfolio_galeria');

                if (!empty($galeria)) {
                  echo '<div id="owl-single-porfolio" class="owl-single-porfolio owl-theme">';
                    foreach ($galeria[0] as $key => $value) {
                        $img_url = wp_get_attachment_image_src($key,'large');
                        echo '<div class="item" style="background-image: url('.$img_url[0].')" ></div>';
                    }
                  echo '</div>';
                  ?>
                  <script type="text/javascript">
                  $(document).ready(function() {

                    $("#owl-single-porfolio").owlCarousel({

                        navigation : true, // Show next and prev buttons
                        slideSpeed : 300,
                        paginationSpeed : 400,
                        singleItem:true

                    });

                  });

                  </script>
                  <?php
                }elseif (has_post_thumbnail()) {
										the_post_thumbnail('large', array( 'class' => 'img-responsive' ));
								}
								 ?>

                <!-- Preview Image -->


                <hr>

                <!-- Post Content -->
                <?php the_content(); ?>

                <hr>

                <!-- Blog Comments -->


            </div>
					<?php endwhile; // end of the loop. ?>


        </div>
        <!-- /.row -->


    </div>
    <!-- /.container -->

<?php get_footer(); ?>
