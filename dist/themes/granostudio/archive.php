<?php
/**
 * The template for displaying archive pages
 *
 *
 */

get_header(); ?>

<!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="<?php echo is_active_sidebar( 'sidebar_blog' ) ? 'col-lg-8' : 'col-lg-8 col-sm-offset-2'; ?>">

                <h1 class="page-header">
                    Blog
										<?php
										the_archive_title( '<small>', '</small>' );
										the_archive_description( '<small>', '</small>' );
										 ?>
                    <!-- <small>Secondary Text</small> -->
                </h1>

                <!-- First Blog Post -->
                <?php
                if( have_posts() ) {
                  while ( have_posts() ) {
                    the_post();
                    echo '<div class="row">';
                    get_template_part( 'template-parts/content-post', 'content-post' );
                    echo '</div>';
                  }
                } else {
                  /* No posts found */
                }
                 ?>

                <!-- Pager -->
                <ul class="pager">

                    <li class="previous"><?php next_posts_link( 'Older posts' ); ?></li>
                    <li class="next"><?php previous_posts_link( 'Newer posts' ); ?></li>

                </ul>
            </div>



            <!-- Blog Sidebar Widgets Column -->
            <?php get_sidebar(); ?>

        </div>
        <!-- /.row -->

        <hr>


    </div>
    <!-- /.container -->

	
<?php get_footer(); ?>
