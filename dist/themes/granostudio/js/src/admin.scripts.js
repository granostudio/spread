// ========= PAGE BUIDER ============== //

var pgb = angular.module("pgb", ["ngMaterial",'ngRoute','ngSanitize',"gridster"]);

pgb.service('modulosAtivos', function(){
  // btn addContent item

  this.modulos = [
    {type: "Páginas"},
    {type: "Imagem"},
    {type: "Texto"},
    {type: "Imagem com texto"},
    {type: "Form. de Contato"},
    {type: "Banner simples"}
  ];
  // verificar conteúdos disponíveis
  this.getModulos = function(){
    if(localized.pluginativos != 0){
      var modulos = JSON.parse(localized.pluginativos);
      console.log( modulos);
      this.modulos.push(modulos);

    }
    return this.modulos;
  }

});


pgb.controller('pgbCtrl', ['$scope','$mdDialog', 'modulosAtivos',
function ($scope, $mdDialog, modulosAtivos){

    // pegar do php o que option do builder
   if(localized.pagebuilder == 0){
     $scope.standardItems = [
       { sizeX: 4, sizeY: 1, row: 0, col: 0 },
       { sizeX: 4, sizeY: 1, row: 0, col: 0 }
     ]
   }else{
     $scope.standardItems = JSON.parse(localized.pagebuilder)
   }



    $scope.gridsterOpts = {
        minRows: 1, // the minimum height of the grid, in rows
        maxRows: 100,
        columns: 4, // the width of the grid, in columns
        // colWidth: 'auto', // can be an integer or 'auto'.  'auto' uses the pixel width of the element divided by 'columns'
        rowHeight: 'match', // can be an integer or 'match'.  Match uses the colWidth, giving you square widgets.
        margins: [5, 5], // the pixel distance between each widget
        // defaultSizeX: 2, // the default width of a gridster item, if not specifed
        // defaultSizeY: 1, // the default height of a gridster item, if not specified
        mobileBreakPoint: 600, // if the screen is not wider that this, remove the grid layout and stack the items
        resizable: {
            enabled: true,
            start: function (event, uiWidget, $element) {
            }, // optional callback fired when resize is started,
            resize: function (event, uiWidget, $element) {
            }, // optional callback fired when item is resized,
            stop: function (event, uiWidget, $element) {
              save();
            } // optional callback fired when item is finished resizing
        },
        draggable: {
            enabled: true, // whether dragging items is supported
            handle: '.ddd', // optional selector for resize handle
            start: function (event, uiWidget, $element) {
            }, // optional callback fired when drag is started,
            drag: function (event, uiWidget, $element) {
            }, // optional callback fired when item is moved,
            stop: function (event, uiWidget, $element) {
              save();
            } // optional callback fired when item is finished dragging
        }
    };


    // btn add item
    $scope.adicionaItem = function () {
        $scope.standardItems.push({ sizeX: 4, sizeY: 1, row: 0, col: 0 })
        // $scope.item.produto = $scope.item.quantidade = '';
        save();
    };



    // btn remover item
    $scope.removeItem = function(index){
        $scope.standardItems.splice(index, 1);
        save();
      }


    // add no menu
    $scope.choiceitens = modulosAtivos.getModulos();




    // save

    function save_edit()
            {
                currently_editing.setAttribute("data-content", encodeURIComponent(document.getElementById("gridster_edit").value));
                save();
            }

    function save()
    {
        // var json_str = JSON.stringify($scope.standardItems);
        document.getElementById("complete_layout_data").value = $scope.standardItems;
    }



}]);
